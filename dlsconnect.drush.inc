<?php
/**
 * @file
 * Drush command for repairing ldap data structure.
 */

/**
 * Implements hook_drush_command().
 */
function dlsconnect_drush_command() {
  $items = array();
  $items['dls-repair-ldap'] = array(
    'callback' => '_dlsconnect_drushrepairldapdata',
    'description' => "Repair all DLS client data and user in LDAP.",
  );
  return $items;
}

/**
 * Implements hook_drush_help().
 */
function dlsconnect_drush_help($section) {
  switch ($section) {
    case 'drush:dls-repair-ldap':
      return dt("Call the DLS connect function _dlsconnect_repairldapdata(). No Argument needed. For each entry a LDAP search is done before writing.");
  }
}

/**
 * Drush command callback.
 */
function _dlsconnect_drushrepairldapdata() {
  $return = _dlsconnect_repairldapdata();
  drush_print($return);
}
