<?php
/**
 * @file
 * Module admin page callbacks.
 */

/**
 * Form builder function.
 */
function dlsconnect_admin_settings() {
  global $cookie_domain;

  $form = array();

  $form['dlsconnect_debug'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable debug mode.'),
    '#default_value' => variable_get('dlsconnect_debug', 0),
  );
  $form['dlsconnect_basedn'] = array(
    '#type' => 'textfield',
    '#title' => t('DLS base DN'),
    '#size' => 50,
    '#maxlength' => 255,
    '#default_value' => variable_get('dlsconnect_basedn', 0),
    '#description' => t('Base DN (like "ou=stage,o=dls,dc=example,dc=com" ) of the LDAP tree where DLS client data can be saved. Make sure that the used LDAP control user DN (maybe in ldapcontrol.module) has the right to write there.'),
  );

  $form['client']['dlsconnect_client'] = array(
    '#type' => 'textfield',
    '#title' => t('client node type'),
    '#size' => 40,
    '#maxlength' => 255,
    '#default_value' => variable_get('dlsconnect_client', 'dlsconnect_client'),
    '#description' => t('machine readable name of the node type to manage dls server clients.'),
  );
  $form['client']['dlsconnect_client_domain'] = array(
    '#type' => 'textfield',
    '#title' => t('CCK textfield for client domain'),
    '#size' => 40,
    '#maxlength' => 255,
    '#default_value' => variable_get('dlsconnect_client_domain', 'field_dlsconnect_domain'),
    '#description' => t('machine readable name of the field containing domain name.'),
  );
  $form['client']['dlsconnect_client_protocol'] = array(
    '#type' => 'textfield',
    '#title' => t('CCK integer field fieldname for protocol'),
    '#size' => 40,
    '#maxlength' => 255,
    '#default_value' => variable_get('dlsconnect_client_protocol', 'field_dlsconnect_protocol'),
    '#description' => t('machine readable fieldname where status is stored in group nodes. This has to be an integer field and single value! Use: 0|http, 1|https'),
  );
  $form['client']['dlsconnect_client_status'] = array(
    '#type' => 'textfield',
    '#title' => t('CCK integer field fieldname for status'),
    '#size' => 40,
    '#maxlength' => 255,
    '#default_value' => variable_get('dlsconnect_client_status', 'field_dlsconnect_status'),
    '#description' => t('machine readable fieldname where status is stored in group nodes. This has to be an integer field and single value! Use: 0|offline, 1|online'),
  );
  $form['client']['dlsconnect_client_id'] = array(
    '#type' => 'textfield',
    '#title' => t('CCK integer field fieldname for Client/Mandant ID'),
    '#size' => 40,
    '#maxlength' => 255,
    '#default_value' => variable_get('dlsconnect_client_id', 'field_dlsconnect_clientid'),
    '#description' => t('machine readable fieldname where status is stored in group nodes. This has to be an integer field and single value! Use: 0|offline, 1|online'),
  );
  $form['client']['dlsconnect_client_roleid'] = array(
    '#type' => 'textfield',
    '#title' => t('CCK integer field fieldname for default role ID'),
    '#size' => 40,
    '#maxlength' => 255,
    '#default_value' => variable_get('dlsconnect_client_roleid', 'field_dlsconnect_roleid'),
    '#description' => t('machine readable fieldname where role id is stored in group nodes. This has to be an integer field and single value! Use: 0|offline, 1|online'),
  );

  $form['group']['dlsconnect_group'] = array(
    '#type' => 'textfield',
    '#title' => t('group node type'),
    '#size' => 40,
    '#maxlength' => 255,
    '#default_value' => variable_get('dlsconnect_group', 'dlsconnect_group'),
    '#description' => t('machine readable name of the node type to manage dls groups.'),
  );
  $form['group']['dlsconnect_group_clientnid'] = array(
    '#type' => 'textfield',
    '#title' => t('CCK nodereference fieldname for referenced client NID'),
    '#size' => 40,
    '#maxlength' => 255,
    '#default_value' => variable_get('dlsconnect_group_clientnid', 'field_dlsconnect_clientnid'),
    '#description' => t('machine readable fieldname where the client NID is stored in group nodes. This has to be a node reference and single value!'),
  );
  $form['group']['dlsconnect_group_id'] = array(
    '#type' => 'textfield',
    '#title' => t('CCK integer fieldname for external group ID'),
    '#size' => 40,
    '#maxlength' => 255,
    '#default_value' => variable_get('dlsconnect_group_id', 'field_dlsconnect_lgid'),
    '#description' => t('machine readable fieldname where the external group ID is stored in group nodes. This has to be an integer field and single value!'),
  );

  $form['dlsconnect_suffix'] = array(
    '#type' => 'textfield',
    '#title' => t('Optional suffix for dls usernames.'),
    '#size' => 40,
    '#maxlength' => 255,
    '#default_value' => variable_get('dlsconnect_suffix', $cookie_domain),
    '#description' => t('This suffix is recommended to avoid user name conflicts on DLS servers. Default is the cookie domain of this website.'),
  );
  $form['dlsconnect_length'] = array(
    '#type' => 'textfield',
    '#title' => t('trim length for block creation'),
    '#size' => 10,
    '#maxlength' => 255,
    '#default_value' => variable_get('dlsconnect_length', '30'),
    '#description' => t('Special value to trim texts. Dots are automatically added. For a custom block and individual themening you can clone the blocks of this module.'),
  );

  return system_settings_form($form);
}
