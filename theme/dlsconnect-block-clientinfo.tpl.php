<?php
/**
 * @file
 * dlsconnect-block-clientinfo.tpl.php
 *
 * Variables available:
 * - $admincontent: an array of data.
 * - $data: an array of data.
 */
?>

<div class="dlsconnect-dlsclientgroup-infoblock">

  <?php if ($admincontent):?>
  <div class="dlsconnect-show-clientinfo">
    <h4><?php echo $admincontent['title1']; ?></h4>
    <ul>
      <li>
        <?php echo $admincontent['soapuser_title']; ?>:
        <?php echo $admincontent['soapuser_data']; ?>
      </li>
      <li>
        <?php echo $admincontent['ldap_title']; ?>:
        <?php echo $admincontent['ldap_data']; ?>
      </li>
    </ul>
  </div>

    <div class="dlsconnect-show-client-cleanup">
      <h4><?php echo $admincontent['title2']; ?></h4>
      <ul>
        <li><?php echo $admincontent['groupsrelated']; ?></li>
        <li><?php echo $admincontent['usersrelated']; ?></li>
      </ul>
    </div>
  <?php endif;?>

  <div class="dlsconnect-dlsclientgroups">
  <h4><?php echo $data['titlerelated']; ?></h4>

  <ul>

  <?php foreach ($data['groupsdata'] as $groupdata):?>
    <li>
    <a title="<?php echo $groupdata['title']; ?>"
       class="dlsconnect-usergroup-item"
       href="<?php echo $groupdata['url']; ?>">
      <span><?php echo $groupdata['link_title']; ?></span></a>
    </li>
  <?php endforeach; ?>

  </ul>
</div>

</div>
