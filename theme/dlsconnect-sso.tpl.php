<?php
/**
 * @file
 * dlsconnect-sso.tpl.php
 *
 * Variables available:
 * - $dlssso: an array of sso informations.
 * - $debugitems: an array of debug informations.
 */
?>
<div class="dlsconnect-client-user-sso">

  <?php  if ($debugitems):?>
    <?php echo theme('item_list', $debugitems); ?>
  <?php endif; ?>

<div class="dlsconnect-client-user-sso-link">
<h3><?php echo $dlssso['title']; ?></h3>

  <p><?php echo $dlssso['info']; ?></p>

<?php if ($dlssso['op'] == TRUE): ?>
  <a href="<?php echo $dlssso['ssourl']; ?>">
  <?php echo $dlssso['ssourl']; ?></a>
<?php endif; ?>
</div>

</div>
