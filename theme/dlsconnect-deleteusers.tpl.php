<?php
/**
 * @file
 * dlsconnect-deleteusers.tpl.php
 *
 * Variables available:
 * - $text: an array of texts.
 * - $data: an array of data.
 */
?>

<div class="dlsconnect-client-deleteusers-confirm">

  <div class="dlsconnect-warning, warning">
    <?php echo $text['confirminfo']; ?>
  </div>'

<div class="dlsconnect-list">
<ul>

<li class="dlsconnect-listitem-clientname">
  <?php echo $text['client']; ?>: <?php echo $data['clienttitle']; ?>
</li>

<li class="dlsconnect-listitem-domain">
  <?php echo $text['domain']; ?>: <?php echo $clientdata['domain']; ?>
</li>

</ul>
</div>

<h3><?php echo $text['question']; ?> </h3>
<?php echo $data['confirmlink']; ?>

</div>
