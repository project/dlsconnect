<?php
/**
 * @file
 * dlsconnect-clientdelete.tpl.php
 *
 * Variables available:
 * - $text: an array of texts.
 * - $data: an array of data.
 */
?>

<div class="dlsconnect-clientuser-delete-confirm">

  <div class="dlsconnect-warning, warning">
    <?php echo $text['confirminfo']; ?>
  </div>'

<div class="dlsconnect-list">
<ul>

<li class="dlsconnect-listitem-clientname">
  <?php echo $text['client']; ?>: <?php echo $data['clienttitle']; ?>
</li>

<li class="dlsconnect-listitem-domain">
  <?php echo $text['domain']; ?>: <?php echo $clientdata['domain']; ?>
</li>

<li class="dlsconnect-listitem-username">
  <?php echo $text['username']; ?>: <?php echo $data['userlink']; ?>
</li>

<li class="dlsconnect-listitem-dlsname">
  <?php echo $text['dlsname']; ?>: <?php echo $data['dlsname']; ?>
</li>

</ul>
</div>

<h3><?php echo $text['question']; ?> </h3>
<?php echo $data['confirmlink']; ?>

</div>
