<?php
/**
 * @file
 * dlsconnect-block-dlsuserccounts.tpl.php
 *
 * Variables available:
 * - $data: an array of data.
 */
?>

<div class="dlsconnect-dlsuserccounts">
  <ul>

  <?php foreach ($data as $client): ?>
    <li>
<a title="<?php echo $client['clientdata']['title1']; ?>"
   class="dlsconnect-sso-link"
   href="<?php echo $client['clientdata']['url1']; ?>">
  <span><?php echo $client['clientdata']['link_text1']; ?></span></a>

<a title="<?php echo $client['clientdata']['title2']; ?>"
   class="dlsconnect-delete-link"
   href="<?php echo $client['clientdata']['url2']; ?>">
  <span> (<?php echo $client['clientdata']['link_text2']; ?>)</span></a>

<div class="dlsconnect-dlsusergroups">
  <ul>
  <?php foreach ($client['clientgroups'] as $group): ?>
<a title="<?php echo $group['title']; ?>" class="dlsconnect-usergroup-item"
   href="<?php echo $group['url']; ?>">
  <span><?php echo $group['link_text']; ?></span></a>
  <?php endforeach; ?>
  </ul>
</div>

    </li>
  <?php endforeach; ?>

  </ul>
</div>
