<?php
/**
 * @file
 * dlsconnect-block-groupinfo.tpl.php
 *
 * Variables available:
 * - $admincontent: a string of data.
 * - $data: an array of data.
 */
?>

<div class="dlsconnect-show-groupinfo">

  <div class="dlsconnect-group-join">
  <?php if ($admincontent): ?>
  <div class="dlsconnect-group-debug">
    <?php echo $admincontent['title1']; ?>
  </div>
  <?php endif; ?>

  <?php if ($data['groupmember'] == TRUE): ?>

    <?php if ($data['infoleave'] != '') : ?>
       <div class="dlsconnect-group-leave-info">
         <?php echo $data['infoleave']; ?>
        </div>
    <?php endif; ?>
    <div class="dlsconnect-group-leave-link">
      <?php echo $data['link']; ?>
    </div>

  <?php endif; ?>

  <?php if ($data['groupmember'] == FALSE): ?>

    <?php if ($data['infojoin'] != ''): ?>
       <div class="dlsconnect-group-join-info">
         <?php echo $data['infojoin']; ?>
        </div>
    <?php endif; ?>

    <div class="dlsconnect-group-join-link">
      <?php echo $data['link']; ?>
    </div>

  <?php endif; ?>
</div>
</div>
