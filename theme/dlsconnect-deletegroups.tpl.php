<?php
/**
 * @file
 * dlsconnect-deletegroups.tpl.php
 * Template to handle layout details of the sso link.
 *
 * Variables available:
 * - $text: an array of texts.
 * - $data: an array of data.
 */
?>

<div class="dlsconnect-client-deletegroups-confirm">

  <div class="dlsconnect-warning, warning">
    <?php echo $text['confirminfo']; ?>
  </div>'

<div class="dlsconnect-list">
<ul>

<li class="dlsconnect-listitem-clientname">
  <?php echo $text['client']; ?>: <?php echo $data['clienttitle']; ?>
</li>

<li class="dlsconnect-listitem-domain">
  <?php echo $text['domain']; ?>: <?php echo $clientdata['domain']; ?>
</li>

</ul>
</div>

<h3><?php echo $text['question']; ?> </h3>
<?php echo $data['confirmlink']; ?>

</div>
