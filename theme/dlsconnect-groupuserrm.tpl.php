<?php
/**
 * @file
 * dlsconnect-groupuserrm.tpl.php
 *
 * Variables available:
 * - $text: an array of texts.
 * - $data: an array of data.
 */
?>

<div class="dlsconnect-groupuser-remove-confirm">

<div class="dlsconnect-list">
<ul>

<li class="dlsconnect-listitem-groupname">
  <?php echo $text['group']; ?>: <?php echo $data['grouplink']; ?>
</li>

<li class="dlsconnect-listitem-username">
  <?php echo $text['username']; ?>: <?php echo $data['userlink']; ?>
</li>

<li class="dlsconnect-listitem-clientname">
  <?php echo $text['client']; ?>: <?php echo $data['clienttitle']; ?>
</li>

<li class="dlsconnect-listitem-domain">
  <?php echo $text['domain']; ?>: <?php echo $clientdata['domain']; ?>
</li>

</ul>
</div>

<h3><?php echo $text['question']; ?> </h3>
<?php echo $data['confirmlink']; ?>

</div>
