<?php
/**
 * @file
 * Page callbacks for managing DLS connections.
 */

/**
 * Menu callback: 'dlsconnect/group/%/add/%'.
 */
function _dlsconnect_page_groupuser_add($groupnid = 0, $uid = 0) {
  if (!(is_numeric($groupnid) && is_numeric($uid))) {
    return drupal_access_denied();
  }

  $node = node_load($groupnid);
  $clientnidfield = variable_get('dlsconnect_group_clientnid', 'field_dlsconnect_clientnid');
  $clientnid = $node->{$clientnidfield}[0]['nid'];

  $clientdata = _dlsconnect_clientdata($clientnid);
  $dlsaccesslevel = _dlsconnect_accesslevel($uid, $clientdata['clientuid']);
  if ($dlsaccesslevel == 0) {
    return drupal_access_denied();
  }

  $success = _dlsconnect_groupuser_add($groupnid, $uid);
  if ($success) {
    drupal_set_message(t('User has joined the DLS group!'), 'status');
  }
  else {
    drupal_set_message(t('Fail on user joining the DLS group!'), 'error');
  }
  drupal_goto('node/' . $groupnid);
}

/**
 * Menu callback: 'dlsconnect/group/%/rm/%'.
 */
function _dlsconnect_page_groupuser_remove($groupnid = 0, $uid = 0) {
  if (!(is_numeric($groupnid) && is_numeric($uid))) {
    return drupal_access_denied();
  }

  $node = node_load($groupnid);
  $clientnidfield = variable_get('dlsconnect_group_clientnid', 'field_dlsconnect_clientnid');
  $clientnid = $node->{$clientnidfield}[0]['nid'];

  $clientdata = _dlsconnect_clientdata($clientnid);
  $dlsaccesslevel = _dlsconnect_accesslevel($uid, $clientdata['clientuid']);
  if ($dlsaccesslevel == 0) {
    return drupal_access_denied();
  }

  $success = _dlsconnect_groupuser_remove($groupnid, $uid);
  if ($success) {
    drupal_set_message(t('User has left the DLS group!'), 'status');
  }
  else {
    drupal_set_message(t('Fail on user leaving the DLS group!'), 'error');
  }

  drupal_goto('node/' . $groupnid);
}

/**
 * Menu callback: 'dlsconnect/group/%/rm/%/confirm'.
 */
function _dlsconnect_page_groupuser_remove_confirm($groupnid = 0, $uid = 0) {
  if (!(is_numeric($groupnid) && is_numeric($uid))) {
    return drupal_access_denied();
  }
  global $user;

  $account = _dlsconnect_getaccount($uid);
  $groupdata = node_load($groupnid);
  $clientnidfield = variable_get('dlsconnect_group_clientnid', 'field_dlsconnect_clientnid');
  $clientnid = $node->{$clientnidfield}[0]['nid'];

  $clientdata = _dlsconnect_clientdata($clientnid);
  $dlsaccesslevel = _dlsconnect_accesslevel($uid, $clientdata['clientuid']);
  if ($dlsaccesslevel == 0) {
    return drupal_access_denied();
  }
  $text = array();
  $data = array();
  if ($user->uid == $uid) {
    $text['question'] = t('Are you sure to leave this DLS group?');
  }
  else {
    $text['question'] = t('Are you sure to remove this user from this DLS group?');
  }

  $text['group'] = t('DLS group');
  $data['grouplink'] = l($groupdata->title, 'node/' . $groupnid);

  $text['username'] = t('username on this system');
  $data['userlink'] = l($account->name, 'user/' . $uid);

  $text['client'] = t('DLS client');
  $data['clienttitle'] = check_plain($clientdata['title']);

  $text['domain'] = t('DLS server domain');
  $data['domain'] = check_plain($clientdata['domain']);

  $data['confirmlink'] = l(t('I understand and confirm.'), 'dlsconnect/group/' . $groupnid . '/rm/' . $uid);

  return theme(array('dlsconnect_groupuserrm'), $text, $data);
}

/**
 * Menu callback: 'dlsconnect/client/%/sso/%'.
 */
function _dlsconnect_page_ssolink($clientnid = 0, $uid = 0) {
  if (!(is_numeric($clientnid) && is_numeric($uid))) {
    return drupal_access_denied();
  }
  $clientdata = _dlsconnect_clientdata($clientnid);
  $dlsaccesslevel = _dlsconnect_accesslevel($uid, $clientdata['clientuid']);
  if ($dlsaccesslevel == 0) {
    return drupal_access_denied();
  }
  $account = _dlsconnect_getaccount($uid);
  $dlssso = _dlsconnect_clientuser_sso($clientnid, $uid);

  $debugitems = array();
  if (user_access('dlsconnect administer global') && variable_get('dlsconnect_debug', 0)) {
    $debugitems[] = $dlssso['clientdata']['title'];
    $debugitems[] = $dlssso['clientdata']['domain'];
    $debugitems[] = l($account->name, 'user/' . $uid);
    $debugitems[] = $dlssso['dlsname'];
  }

  return theme(array('dlsconnect_sso'), $dlssso, $debugitems);
}

/**
 * Menu callback: 'dlsconnect/client/%/delete/%'.
 */
function _dlsconnect_page_clientuser_delete($clientnid = 0, $uid = 0) {
  if (!(is_numeric($clientnid) && is_numeric($uid))) {
    return drupal_access_denied();
  }

  $clientdata = _dlsconnect_clientdata($clientnid);
  $dlsaccesslevel = _dlsconnect_accesslevel($uid, $clientdata['clientuid']);
  if ($dlsaccesslevel == 0) {
    return drupal_access_denied();
  }

  $db = _dlsconnect_clientuser_delete($clientnid, $uid);
  if ($db) {
    drupal_set_message(t('User deleted from DLS client!'), 'status');
  }
  else {
    drupal_set_message(t('Fail on user deleting from DLS client!'), 'error');
  }
  drupal_goto('user/' . $uid);
}

/**
 * Menu callback: 'dlsconnect/client/%/delete/%/confirm'.
 */
function _dlsconnect_page_clientuser_delete_confirm($clientnid = 0, $uid = 0) {
  if (!(is_numeric($clientnid) && is_numeric($uid))) {
    return drupal_access_denied();
  }
  global $user;

  $account = _dlsconnect_getaccount($uid);

  // Check if user already clientuser.
  $clientuser = _dlsconnect_clientuser_data($clientnid, $uid);
  $userlogin = $clientuser['dlsname'];

  $clientdata = _dlsconnect_clientdata($clientnid);
  $dlsaccesslevel = _dlsconnect_accesslevel($uid, $clientdata['clientuid']);
  if ($dlsaccesslevel == 0) {
    return drupal_access_denied();
  }
  $text = array();
  $data = array();
  if ($user->uid == $uid) {
    $text['question'] = t('Are you sure to delete your DLS client account?');
  }
  else {
    $text['question'] = t('Are you sure to delete this user from this DLS client?');
  }

  $text['confirminfo'] = t('If you confirm all data of the DLS client user are deleted on the remote DLS server and all DLS group entries in this system are deleted!');

  $text['client'] = t('DLS client');
  $data['clienttitle'] = check_plain($clientdata['title']);

  $text['domain'] = t('DLS server domain');
  $data['domain'] = check_plain($clientdata['domain']);

  $text['dlsname'] = t('username on DLS client');
  $data['dlsname'] = $userlogin;


  $data['confirmlink'] = l(t('I understand and confirm this complete deletion.'), 'dlsconnect/client/' . $clientnid . '/delete/' . $uid);

  return theme(array('dlsconnect_clientdelete'), $text, $data);
}

/**
 * Menu callback: 'dlsconnect/client/%/delete/groups'.
 */
function _dlsconnect_page_client_deletegroups($clientnid = 0) {
  if (!(is_numeric($clientnid))) {
    return drupal_access_denied();
  }
  $clientdata = _dlsconnect_clientdata($clientnid);
  global $user;
  $dlsaccesslevel = _dlsconnect_accesslevel($user->uid, $clientdata['clientuid']);
  if ($dlsaccesslevel < 2) {
    return drupal_access_denied();
  }
  _dlsconnect_dlsclientgroups_alldelete($clientnid);
  drupal_set_message(t('Groups deleted from DLS client!'), 'status');
  drupal_goto('node/' . $clientnid);
}

/**
 * Menu callback: 'dlsconnect/client/%/delete/groups/confirm'.
 */
function _dlsconnect_page_client_deletegroups_confirm($clientnid = 0) {
  if (!(is_numeric($clientnid))) {
    return drupal_access_denied();
  }
  global $user;

  $clientdata = _dlsconnect_clientdata($clientnid);
  $dlsaccesslevel = _dlsconnect_accesslevel($user->uid, $clientdata['clientuid']);
  if ($dlsaccesslevel < 2) {
    return drupal_access_denied();
  }
  $text = array();
  $data = array();

  $text['question'] = t('Are you sure to delete DLS client groups?');

  $text['confirminfo'] = t('If you confirm all group data including group nodes of the DLS client are deleted!');

  $text['client'] = t('DLS client');
  $data['clienttitle'] = check_plain($clientdata['title']);

  $text['domain'] = t('DLS server domain');
  $data['domain'] = check_plain($clientdata['domain']);

  $data['confirmlink'] .= l(t('I understand and confirm this complete deletion.'), 'dlsconnect/client/' . $clientnid . '/delete/groups');

  return theme(array('dlsconnect_deletegroups'), $text, $data);
}
/**
 * Menu callback: 'dlsconnect/client/%/delete/users'.
 */
function _dlsconnect_page_client_deleteusers($clientnid = 0) {
  if (!(is_numeric($clientnid))) {
    return drupal_access_denied();
  }
  $clientdata = _dlsconnect_clientdata($clientnid);
  global $user;
  $dlsaccesslevel = _dlsconnect_accesslevel($user->uid, $clientdata['clientuid']);
  if ($dlsaccesslevel < 2) {
    return drupal_access_denied();
  }
  $delete = _dlsconnect_dlsclientusers_alldelete($clientnid);
  if ($delete == TRUE) {
    drupal_set_message(t('Users deleted from DLS client!'), 'status');
  }
  else {
    drupal_set_message(t('Error on user deletion from DLS client!'), 'error');
  }
  drupal_goto('node/' . $clientnid);
}

/**
 * Menu callback: 'dlsconnect/client/%/delete/users/confirm'.
 */
function _dlsconnect_page_client_deleteusers_confirm($clientnid = 0) {
  if (!(is_numeric($clientnid))) {
    return drupal_access_denied();
  }
  global $user;

  $clientdata = _dlsconnect_clientdata($clientnid);
  $dlsaccesslevel = _dlsconnect_accesslevel($user->uid, $clientdata['clientuid']);
  if ($dlsaccesslevel < 2) {
    return drupal_access_denied();
  }
  $text = array();
  $data = array();

  $text['question'] = t('Are you sure to delete DLS client users?');

  $text['confirminfo'] = t('If you confirm all client users including group nodes of the DLS client are deleted!');

  $text['client'] = t('DLS client');
  $data['clienttitle'] = check_plain($clientdata['title']);

  $text['domain'] = t('DLS server domain');
  $data['domain'] = check_plain($clientdata['domain']);

  $output .= l(t('I understand and confirm this complete deletion.'), 'dlsconnect/client/' . $clientnid . '/delete/users');

  return theme(array('dlsconnect_deleteusers'), $text, $data);
}
