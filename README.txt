ABOUT
-----
DLSconnect allows to integrate drupal with DLS (Distance Learning System)
Servers. http://www.ets-online.de/systems/dls-distancelearningsystem/

Some customizing can only be made by hooks in a custom module.
Some functions are only available with drush: http://drupal.org/project/drush

The API of LDAPcontrol module can be used to control data structures on a LDAP
server if installed. With a custom hook the LDAP server controling can be
handled on another way maybe direct with PHP LDAP functions.

The module is developed and tested with Open LDAP (http://www.openldap.org/) on
Ubuntu 10.4 LTS.


FEATURES
--------
+ Special short time password handling to secure SOAP communications.
+ Drupal users can join DLS groups by assigning to group nodes.
+ DLS client users are automatic created with joining of a DLS group of its DLS
  client.
+ DLS access can be handles with content access rules maybe with organic
  groups.
+ Single sign on/one time login links as only way to access DLS server vor
  drupal DSL client user.
+ Complete rebuild of DLS Data in LDAP with drush.
+ customize actions e.g. status, warning and error with
  hook_dlsconnect_actions()


REQUIREMENTS
------------
+ NUSOAP (http://sourceforge.net/projects/nusoap/)
+ objectclasses "person" and "organizationalUnit" have to be installed on LDAP
  server.
+ CCK module


INSTALLATION
------------
Install Nusoap like this sites/all/libraries/nusoap/lib/nusoap.php
Add this module to modules folder and enable.
Install ldapcontrol.module or use an own LDAP integration.
See /admin/settings/dlsconnect for default node type names and cck fields.
Create or use existing node types and cck fields if not exist.


CONFIGURATION
-------------
If the default user data handling doesn't fit your needs can use
hook_dlsconnect_userfields().
Maybe to change the node types and CCK fields for DLS clients and DLS groups.
For each DLS Client special configuration has to made by DLS administrators.


USAGE FOR DLS ADMINISTRATORS
----------------------------
1. Create a client node and fill out the special CCK fields:
   + domain name of the DLS Server
   + the ID of the client on the DLS Server
   + the Role ID on the DLS Server for new DLS users
   + the status of the client in the system. If offline: the one-time-login
     functionality is blocked
2. Create a special user on the DLS client for SOAP operations with the
     possibility to authentcate via LDAP
   The name of the individual soap which is shown on the client node view.
3. Configure LDAP authentication settings of the client on the DLS server.
   + Standard LDAP server data including domain and LDAP user which is needed
     by all clients.
   + Each client node provides an indivudual search base in LDAP (shown on
     client node view).
4. On saving of the client node the internal client data are updated.
     If warnings are shown mabe something is wrong in settings above.
5. Create at least one DLS group node per client and fill out the special CCK
   fields:
   + The node reference to the related client node.
   + The group ID on the DLS Server.

AUTHOR
------
Carsten Logemann (http://drupal.org/user/218368)
