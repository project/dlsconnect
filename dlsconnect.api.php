<?php

/**
 * @file
 * Hooks provided by DLSconnect.
 *
 * All hooks are intended for a special customisation. If you need a special
 * hook especially for drupal_alter() please contact the maintainer.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Routing LDAP operations.
 *
 * This is very special hook if you don't want to use the LDAPcontrol module
 * maybe for conneting to an LDAP Server directly via PHP functions.
 *
 * This hook is completly building the ldap data. You should only modify this
 * if you understnd what your LDAP server and the DLS Server needs.
 */
function hook_dlsconnect_ldapactions($type, $data, $op) {
  $dlsbasedn = variable_get('dlsconnect_basedn', 0);

  $return = array();

  $ldapdata = array();
  $basedn = '';
  $name_attr = '';
  $name = '';

  if ($type == 'group') {
    $name_attr = 'ou';
    $name = $data['groupname'];
    $basedn = $dlsbasedn;
    if ($op == 'create') {
      $ldapdata["objectclass"][] = "organizationalUnit";
    }
    $ldapdata["ou"][] = $data['groupname'];
  }
  elseif ($type == 'user') {
    $name_attr = 'cn';
    $name = $data['username'];
    $basedn = 'ou=' . $data['groupname'] . ',' . $dlsbasedn;
    if ($op == 'create') {
      $ldapdata['objectclass'][] = 'Person';
    }
    if ($op == 'pass') {
      $ldapdata['userPassword'][] = $data['pass'];
      $op = 'update';
    }
    else {
      $ldapdata['cn'][] = $data['username'];
      $ldapdata['surname'][] = $data['username'];
      $ldapdata['userPassword'][] = $data['pass'];
    }
  }
  $entry = $name_attr . '=' . $name;

  $return = _ldapcontrol_ldapoperation($op, $entry, $basedn, $ldapdata);

  return $return;
}

/**
 * Maps user information to user accounts in DLS server.
 *
 * Per default Firstname and Lastname is set to drupal username and
 * gender is set to "2" female. Because standard user data in drupal are
 * very small. If you have more data about your users maybe with profiles
 * you can map these data with this hook.
 */
function hook_dlsconnect_userfields($uid) {
  $userfields = array();
  $account = _dlsconnect_getaccount($uid);
  $userfields['Email'] = $account->mail;
  $userfields['Gender'] = 2;
  $userfields['Firstname'] = $account->name;
  $userfields['Lastname'] = $account->name;
  return $userfields;
}


/**
 * Receiving information of several operaions in drupal db, LDAP and DLS.
 *
 * Per defaulr only errors are written in watchdog. With this hook you can
 * also use status and warning messages and more actions than watchdog.
 *
 * @param string $op
 *   A string containing what operation is made:
 *   - error.
 *   - warning.
 *   - status.
 * @param string $type
 *   A string containing type of database involved:
 *   - dls: External DLS server.
 *   - ldap: External LDAP server.
 *   - db: Internal database e.g. MySQL.
 *
 * @param array $data
 *   An associative array containing:
 *   - clientnid: The nid of the client node.
 *   - uid: The uid of the concerning user.
 *   - msg: A message string.
 */
function hook_dlsconnect_actions($op, $type, $data = array()) {
  if ($op == 'error') {
    watchdog('dlsconnect', $data['msg'], NULL, WATCHDOG_ERROR, $link = NULL);
  }
}

/**
 * @} End of "addtogroup hooks".
 */
