<?php
/**
 * @file
 * dlsconnect.install
 */


/**
 * Implements hook_install().
 */
function dlsconnect_install() {
  drupal_install_schema('dlsconnect');
}

/**
 * Implements hook_uninstall().
 */
function dlsconnect_uninstall() {
  drupal_uninstall_schema('dlsconnect');

  variable_del('dlsconnect_debug');
  variable_del('dlsconnect_basedn');

  variable_del('dlsconnect_client');
  variable_del('dlsconnect_client_domain');
  variable_del('dlsconnect_client_protocol');
  variable_del('dlsconnect_client_status');
  variable_del('dlsconnect_client_id');
  variable_del('dlsconnect_client_roleid');

  variable_del('dlsconnect_course');
  variable_del('dlsconnect_course_clientnid');
  variable_del('dlsconnect_course_id');

  variable_del('dlsconnect_suffix');
  variable_del('dlsconnect_length');
}

/**
 * Implements hook_schema().
 */
function dlsconnect_schema() {
  $schema = array();

  $schema['dlsconnect_systemid'] = array(
    'description' => 'To store an ExternalSystemId of an DLS server for SOAP-Connections',
    'fields' => array(
      'systemid' => array(
        'description' => 'The SystemID given by DLS server.',
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
        'default' => '1'),
      'domain' => array(
        'description' => 'The domain name of a DLS server',
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
        'default' => ''),
      'created' => array(
        'description' => 'The Unix timestamp when the DLS server was added .',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0),
      'changed' => array(
        'description' => 'The Unix timestamp when the ExternalSystemId was changed.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0),
    ),
    'primary key' => array('domain'),
  );

  $schema['dlsconnect_clientusers'] = array(
    'description' => 'To store users of DLS server.',
    'fields' => array(
      'nid' => array(
        'description' => 'The ID of a DLS mandant/client node.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0),
      'uid' => array(
        'description' => 'The Drupal ID of the user.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0),
      'dlsuid' => array(
        'description' => 'The DLS ID of the user.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0),
      'dlsname' => array(
        'description' => 'The username of a user on a DLS server.',
        'type' => 'varchar',
        'length' => 128,
        'not null' => TRUE,
        'default' => ''),
      'status' => array(
        'description' => 'Access to DLS server enabled or disabled.',
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        'default' => 0),
      'soapuser' => array(
        'description' => 'Mark this user account as soap user for special operations.',
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        'default' => 0),
      'created' => array(
        'description' => 'The Unix timestamp when the user was assigned to the DLS server.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0),
      'changed' => array(
        'description' => 'The Unix timestamp when this entry was changed.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0),
    ),
    'primary key' => array('dlsname'),
  );

  $schema['dlsconnect_groupusers'] = array(
    'description' => 'To store users of groups on DLS server',
    'fields' => array(
      'nid' => array(
        'description' => 'The ID of a DLS Group Node.',
        'type'        => 'int',
        'size'        => 'normal',
        'not null'    => TRUE,
        'default'     => 0),
      'uid' => array(
        'description' => 'The Drupal ID of the user.',
        'type'        => 'int',
        'size'      => 'normal',
        'not null'    => TRUE),
      'created' => array(
        'description' => 'The Unix timestamp when the user was assigned to the DLS group.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0),
    ),
  );
  return $schema;
}


/**
 * Implements hook_requirements().
 */
function dlsconnect_requirements($phase) {
  $t = get_t();
  $path = libraries_get_path('nusoap');

  $requirements['nusoap'] = array(
    'title' => $t('NUSOAP'),
    'value' => $t('0.9.5'),
  );

  if (!file_exists($path . '/lib/nusoap.php')) {
    $requirements['nusoap']['value'] = $t('Not found or wrong version');
    $requirements['nusoap']['description'] = $t('Nusoap is not installed properly. See README.txt for details.');
    $requirements['nusoap']['severity'] = REQUIREMENT_ERROR;
  }

  return $requirements;
}
